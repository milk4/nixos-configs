{ config, pkgs, ... }:

# zduchac's PERSONAL configration.nix
# do NOT steal

{
	imports = [ ./hardware-configuration.nix ];

	boot =
	{
		loader.grub =
		{
			enable = true;
			version = 2;
			device = "/dev/sda";
		};

		extraModprobeConfig =
		''
			options thinkpad_acpi fan_control=1
		'';
	};

	system =
	{
		stateVersion = "16.09";

		autoUpgrade =
		{
			enable = true;
		};
	};

	networking =
	{
		hostName = "hostname";
		networkmanager.enable = true;
	};

	i18n = 
	{
		consoleFont = "Lat2-Terminus16";
		consoleKeyMap = "pl";
		defaultLocale = "pl_PL.UTF-8";
	};

	time =
	{
		timeZone = "Europe/Warsaw";
	};

	environment.systemPackages = with pkgs;
	[
		aria2
		irssi

		gst_plugins_good	# The Good,
		gst_plugins_bad		# the Bad,
		gst_plugins_ugly	# and the Ugly

		pidgin
		pidginotr

		firefox

		wine
	];

	services =
	{
		xserver =
		{
			enable = true;
			layout = "pl";

			displayManager =
			{
				gdm.enable = true;
			};
			
			desktopManager =
			{
				gnome3.enable = true;
				default = "gnome3";
			};
		};
	};

	users =
	{
		extraUsers.milka =
		{
			isNormalUser = true;

			uid = 1000;
			description = "Milka";
			extraGroups = [ "wheel" "networkmanager" "audio" "video" ];
		};
	};
}
